#ifndef _VERSION_H_
#define _VERSION_H_

#define DATE __DATE__
#define TIME __TIME__

#ifndef VERSION_MAJOR
#define VERSION_MAJOR  0
#endif
#ifndef VERSION_MINOR
#define VERSION_MINOR  1
#endif
#ifndef VERSION_REVIS
#define VERSION_REVIS  0
#endif

#define TO_STR(A) #A
#define VERSION_STR(A, B, C) TO_STR(A) "." TO_STR(B) "." TO_STR(C)

#ifndef VERSION
#define VERSION VERSION_STR(VERSION_MAJOR, VERSION_MINOR, VERSION_REVIS)
#endif

#ifndef GIT_COMMIT
// if there is no git commit this placeholder will be used.
#define GIT_COMMIT "00000000"
#endif

#endif