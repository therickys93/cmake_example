#include <stdio.h>
#include <stdarg.h>
#include <signal.h>
#include <stdlib.h>
#include "mystdio.h"
#include "version.h"

#define RED   "\033[0;31m"
#define GREEN "\033[0;32m"
#define WHITE "\033[0m"

#define UNUSED(x) (void)(x)

static void red_printf(char *format, ...)
{
	va_list args;
	va_start(args, format);
	myprintf("%s", RED);
	myprintf(format, args);
	myprintf("%s", WHITE);
	va_end(args);
}

static void green_printf(char *format, ...)
{
	va_list args;
	va_start(args, format);
	myprintf("%s", GREEN);
	myprintf(format, args);
	myprintf("%s", WHITE);
	va_end(args);
}

int main(int argc, char **argv)
{
	UNUSED(argc);
	myprintf("%sHello Red!%s\n", RED, WHITE);
	myprintf("%sHello Green!%s\n", GREEN, WHITE);
	red_printf("Hello Red!\n");
	green_printf("Hello Green!\n");
	green_printf("GO ON!\n");
	printf("%s version %s-g%s compiled on %s at %s\n", argv[0], VERSION, GIT_COMMIT, DATE, TIME);
	return 0;
}
