#include "mystdio.h"
#include <stdarg.h>
#include <stdio.h>

void myprintf(char *format, ...)
{
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
}
