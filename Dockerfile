FROM registry.gitlab.com/therickys93/cmake_example:makeimage AS builder
ADD . /code
WORKDIR /code
RUN cd compile && cmake .. && make

FROM ubuntu
COPY --from=builder /code/compile/example_app .
